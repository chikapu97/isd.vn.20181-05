##Team: ISD.VN.20181-05

##Members:

	Phạm Đình Tuấn Anh - 20150159
	
	Vũ Tuấn Anh - 20150207
	
	Nguyễn Văn Đoàn - 20150949


## Homework 06–GUI Design

	Phạm Đình Tuấn Anh: I, II.11, II.12, II.13, II.14, Create Mock ups by Invisionapp

	Vũ Tuấn Anh: II.1, II.2, II.3, II.4, II.5, Design the screen transition diagram

	Nguyễn Văn Đoàn: II.6, II.7, II.8, II.9, Design the screen transition diagram



	Phạm Đình Tuấn Anh review Design for each screen

	Nguyễn Văn Đoàn review Design the screen transition diagram

	Vũ Tuấn Anh review Create Mock ups by Invisionapp