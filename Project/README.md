##Team: ISD.VN.20181-05

##Members:

	Phạm Đình Tuấn Anh - 20150159
	
	Vũ Tuấn Anh - 20150207
	
	Nguyễn Văn Đoàn - 20150949


## Final Project

### Code

	Phạm Đình Tuấn Anh: UC007, UC008, UC009, UC010, UC015, UC016, UC018 (100%)

	Vũ Tuấn Anh: UC001, UC002, UC003, UC011, UC012, UC017, UC020 (100%)

	Nguyễn Văn Đoàn: UC004, UC005, UC006, UC013, UC014, UC019, UC021 (100%)

### Report

	Phạm Đình Tuấn Anh: Test case (100%)

	Vũ Tuấn Anh: SDD, video demo (100%)

	Nguyễn Văn Đoàn: SRS (100%)
 
### Review

	Phạm Đình Tuấn Anh reviewed SRS, SDD

	Nguyễn Văn Đoàn reviewed Video, test case

	Vũ Tuấn Anh reviewed Code