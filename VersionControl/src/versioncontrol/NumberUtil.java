package versioncontrol;

public class NumberUtil {
    private boolean isPrimeNumber(int number){
        if(number < 2) return false;
        else{
            int sqrtOfNumber = (int) Math.sqrt(number);
            for(int i = 2; i < sqrtOfNumber; i++){
                if(number % i == 0) return false;
            }
            return true;
        }
    }
    
    public int isPrimeOrCompositeNumber(int number){
        if(isPrimeNumber(number)) return 1;
        else if(number > 1) return 2;
        else return 0;
    }
    
    public boolean isSquareNumber(int number){
        int sqrtOfNumber = (int) Math.sqrt(number);
        if(number % sqrtOfNumber == 0) return true;
        else return false;
    }
    
    public boolean isPerfectNumber(int number){
        int sum = 0;
        for (int i = 1; i < number; i++)
            if(number % i == 0) sum = sum + i;
        if(sum == number) return true;
        else return false;
    }
}
