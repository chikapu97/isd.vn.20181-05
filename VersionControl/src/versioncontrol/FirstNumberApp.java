package versioncontrol;

import java.util.Scanner;

public class FirstNumberApp {
    private int number;
    
    public void setNumber(int number){
        this.number = number;
    }
    
    public int getNumber(){
        return this.number;
    }
    
    public void inputNumber(){
        Scanner sc = new Scanner(System.in);
        int number;
        do{
            System.out.print("Moi ban nhap mot so nguyen: ");
            number = sc.nextInt();
        } while(number != (int) number);
        setNumber(number);
    }
    
    public void numberInfo(){
        NumberUtil userNumber = new NumberUtil();
        switch(userNumber.isPrimeOrCompositeNumber(getNumber())){
            case 1:
                System.out.println(getNumber() + " la so nguyen to");
                break;
            case 2:
                System.out.println(getNumber() + " la hop so");
                break;
        }
        
        if(userNumber.isSquareNumber(getNumber())) System.out.println(getNumber() + "la so chinh phuong");
        else System.out.println(getNumber() + " khong phai la so chinh phuong");
        
        if(userNumber.isPerfectNumber(getNumber())) System.out.println(getNumber() + "la so hoan hao");
        else System.out.println(getNumber() + " khong phai la so hoan hao");
    }
}
