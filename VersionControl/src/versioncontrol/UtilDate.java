package versioncontrol;

import java.util.Date;

public class UtilDate {
    public boolean  checkDate(int  day,  int  month,  int  year){
        switch(month){
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                if(day < 1 || day > 31) return false;
                else return true;
            case 4:
            case 6:
            case 9:
            case 11:
                if(day < 1 || day > 30) return false;
                else return true;
            case 2:
                if(year % 4 == 0 && year % 400 == 0 && year % 100 != 0){
                    if(day < 1 || day > 29) return false;
                    else return true;
                } else{
                    if(day < 1 || day > 29) return false;
                    else return true;
                }
            default: return false;
        }
    }
    
    public int calculateAge(int year) throws Exception{
        Date currentDate = new Date();
        int currentYear;
        currentYear = currentDate.getYear() + 1900;
        if ((currentYear - year) < 0) throw new Exception();
        return currentYear - year;
    }
}
