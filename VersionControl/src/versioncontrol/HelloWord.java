package versioncontrol;

import java.util.Scanner;

public class HelloWord {
    private String name;
    private int day;
    private int month;
    private int year;
    
    public String getName(){
        return this.name;
    }
    
    public int getDay(){
        return this.day;
    }
    
    public int getMonth(){
        return this.month;
    }
    
    public int getYear(){
        return this.year;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setDay(int day){
        this.day = day;
    }
    
    public void setMonth(int month){
        this.month = month;
    }
    
    public void setYear(int year){
        this.year = year;
    }
    
    public void inputNameAndDate(){        
        Scanner sc = new Scanner(System.in);
        String name; 
        int day, month, year;
        System.out.print("Moi ban nhap ten: ");
        name = sc.nextLine(); setName(name);        
        System.out.print("Moi ban nhap ngay sinh: ");
        day = sc.nextInt(); setDay(day);
        System.out.print("Moi ban nhap thang sinh: ");
        month = sc.nextInt(); setMonth(month);
        System.out.print("Moi ban nhap nam sinh: ");
        year = sc.nextInt(); setYear(year);
    }
    
    public void checkDate(){
        UtilDate date = new UtilDate();
        while(!date.checkDate(getDay(), getMonth(), getYear())){
            Scanner sc = new Scanner(System.in);
            int day, month, year; 
            System.out.print("Moi ban nhap ngay sinh: ");
            day = sc.nextInt(); 
            System.out.print("Moi ban nhap thang sinh: ");
            month = sc.nextInt(); 
            System.out.print("Moi ban nhap nam sinh: ");
            year = sc.nextInt();
        }
        setDay(day);
        setMonth(month);
        setYear(year);
    }
    
    public void sayHello() throws Exception{
        UtilDate date = new UtilDate();
        System.out.println("Hello " + this.name);
        System.out.println("Tuoi cua ban la: " + date.calculateAge(getYear()));
    }
}
